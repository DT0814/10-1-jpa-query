package com.twuc.webApp.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;

import javax.persistence.QueryHint;
import java.util.List;

public interface ProductRepository extends JpaRepository<Product, String> {
    // TODO
    //
    // 如果需要请在此添加方法。
    //
    // <--start--
    @Query("select p from Product p ,ProductLine pl where p.productLine = pl and pl.textDescription like '%hobby%'")
    List<Product> findProductLintContents(String hobby);

    @Query("select p from Product p where p.quantityInStock between :begin and :end")
    List<Product> findQuantityBetween(@Param("begin") Short begin, @Param("end") Short end);

    @Query("select p from Product p where p.quantityInStock between :begin and :end order by p.productCode")
    List<Product> findQuantityBetweenSort(@Param("begin") Short begin, @Param("end") Short end);

    @QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value = "true") })
    @Query(value = "select p from Product p where p.quantityInStock between :begin and :end order by p.productCode")
    List<Product> findQuantityBetweenSortLimit(@Param("begin") Short begin, @Param("end") Short end,Pageable pageable);

    @Query(value = "select p from Product p where p.quantityInStock between :begin and :end order by p.productCode")
    Page<Product> findQuantity(@Param("begin") Short begin, @Param("end") Short end, Pageable pageable);

    // --end-->
}
